{
    "_id": "00001000100000",
    "index": 9,
    "guid": "284b6e63-f4e1-4820-87b4-a3ff283235e9",
    "isActive": true,
    "balance": "$20 0000.56",
    "picture": "http://placehold.it/32x32",
    "age": 32,
    "eyeColor": "brown",
    "name": "Cunningham Peterson",
    "gender": "male",
    "company": "LIQUIDOCSZZZZZ",
    "email": "cunninghampeterson@liquidoc.com",
    "phone": "+1 (829) 440-3721",
    "address": "179 Middleton Street, Babb, Puerto Rico, 3162",
    "about": "Quis ea qui ad adipisicing enim et aute. Amet enim est nulla laborum ipsum amet. Aute voluptate reprehenderit ut sunt qui dolor sit veniam velit anim voluptate reprehenderit quis ea. Nostrud nisi anim ullamco nisi. Et aliquip eiusmod fugiat sint deserunt anim labore. Excepteur sunt do quis Lorem laboris ex occaecat sit nisi. Culpa veniam incididunt laborum dolore sit ea voluptate aliqua occaecat Lorem commodo qui magna amet.\r\n",
    "registered": "2016-06-30T12:48:52 -03:00",
    "latitude": -45.217248,
    "longitude": -167.77256900000000,
    "tags": [
      "excepteur",
      "enim",
      "duis",
      "in",
      "dolor",
      "veniam",
      "adipisicing"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Dodson Ivanov"
      },
      {
        "id": 1,
        "name": "Simon Taylor"
      },
      {
        "id": 2,
        "name": "Candice Wyatt"
      }
    ],
    "greeting": "Hello, Cunningham Peterson! You have 4 unread messages.",
    "favoriteFruit": "banana"
  }
